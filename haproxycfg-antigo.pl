#!/usr/bin/perl
#

use strict;

my $haproxy_config_file = "/etc/haproxy/haproxy.cfg";
my $global_config = 1;
my @global_config;
my $defaults_config = 1;
my @defaults_config;
my $acl_config = 1;
my @acl_config;
my $use_backend_config = 1;
my @use_backend_config;
my $backend_config = 1;
my @backend_config;
my @new_backend_config;
my $backend_exists = 1;
my $farm_found = 1;
my $farm_found_once = 1;
my $farm_exists = 1;
my $frontend;

#vem do cgi
my $farm_name = 'backlot_api_farma';
my $backend_name = 'backlot-api-12.ig.infra';
#my $backend_name = 'backlot-api-13.ig.infra';
my $backend_port = '80';
my $backend_weight = 1;
my $backend_check = 'check';
my $operation = 'add';
#my $operation = 'del';
my $object_type = 'server';
#my $object_type = 'farm';
my @server_id = split(/\./,$backend_name);

open FH,"< $haproxy_config_file" || die "Cannot read $haproxy_config_file";
while ( my $line = <FH> ) {
  chomp($line);
  if ( $line =~ m/^global$/o ) {
    $global_config = 0;
  } elsif ( $line =~ m/^defaults$/o ) {
    $defaults_config = 0;
    $global_config = 1;
  } elsif ( $line =~ m/^frontend/o ) {
    $defaults_config = 1;
    $frontend = $line;
  } elsif ( $line =~ m/^\s+acl/o ) {
    $acl_config = 0;
  } elsif ( $line =~ m/^\s+use_backend/o ) {
    $acl_config = 1;
    $use_backend_config = 0;
  } elsif ( $line =~ m/^\s+backend/o ) {
    $use_backend_config = 1;
    $backend_config = 0;
  }
  if ( $global_config == 0 ) {
    push(@global_config,$line);
  }
  if ( $defaults_config == 0 ) {
    push(@defaults_config,$line);
  }
  if ( $acl_config == 0 ) {
    push(@acl_config,$line);
  }
  if ( $use_backend_config == 0 ) {
    push(@use_backend_config,$line);
  }
  if ( $backend_config == 0 ) {
    push(@backend_config,$line);
  }
  #print $line."\n";
}
close FH;

if ( grep(/backend $farm_name/,@backend_config) ) {
  $farm_exists = 0;
}

if ( grep(/server @server_id[0] $backend_name:$backend_port/,@backend_config) ) {
  $backend_exists = 0;
}

foreach my $line ( @backend_config ) {
    my $print_line = 0;
    if ( $operation eq 'add' ) {
      if ( $object_type eq 'farm' ) {
        if ( $farm_exists == 0 ) {
          die "Farm $farm_name already exists";
        } else {
          print "add farm\n";
        }
      } elsif ( $object_type eq 'server' ) {
        if ( $backend_exists == 0 ) {
          die "Backend $backend_name already exists";
        } else {
          print "add server\n";
        }
      }
#    } elsif ( $operation eq 'del' ) {
#      if ( $object_type eq 'farm' ) {
#
#      } elsif ( $object_type eq 'server' ) {
#
#      }

    }   




    if ( $line =~ m/backend $farm_name/o ) {
      $farm_found = 0;
      $farm_found_once = 0;
    } elsif ( $line =~ m/backend/o ) {
      $farm_found = 1;
    }
    if ( $farm_found == 0 ) {
      if ( $object_type eq "server" ) {
        if ( $operation eq "add" ) {
          if ( grep(/server @server_id[0] $backend_name:$backend_port/,@backend_config) ) {
              die "Server already in farm";
          } else {
            if ( $line =~ m/server/o ) {
              my $new_line = "               server @server_id[0] $backend_name:$backend_port weight $backend_weight $backend_check";
              push(@new_backend_config,$new_line);
              $farm_found = 1;
            }
          }
        } elsif ( $operation eq "del" ) {
          if ( $line =~ m/server\s@server_id[0]\s$backend_name:$backend_port/o ) {
            $print_line = 1;
          }
        }
      } elsif ( $object_type eq "farm" ) {
        if ( $operation eq "add" ) { 
          die "Farm $farm_name already exists";
        }
      }
    }
    push(@new_backend_config,$line) if $print_line != 1;
}

#print "@global_config\n\n\n";
foreach my $line ( @global_config ) {
  print $line."\n";
}
print "\n";
#print "@defaults_config\n\n\n";
foreach my $line ( @defaults_config ) {
  print $line."\n";
}
print "\n";
print $frontend."\n";;
print "\n";
#print "@acl_config\n\n\n";
foreach my $line ( @acl_config ) {
  print $line."\n";
}
print "\n";
#print "@use_backend_config\n\n\n";
foreach my $line ( @use_backend_config ) {
  print $line."\n";
}
print "\n";

foreach my $line ( @new_backend_config ) {
  print $line."\n";
}

exit 0;