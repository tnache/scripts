#!/bin/bash
#
# Script to create a PPTP VPN with Super Free VPN and add routes to access netflix from it.
#
# ChangeLog:
#
# 20130401 => Created
#

OUTPUT_FILE="/tmp/output"
GW_FILE="/tmp/gw"

echo "Backuping chap-secrets"
mv /etc/ppp/chap-secrets /etc/ppp/chap-secrets.$(date +%Y%m%d)
read -p "Please, digit password for freevpn: " PASSWORD
pptpsetup --create superfreevpn --server superfreevpn.com --username free --password ${PASSWORD} --encrypt --start > ${GW_FILE} &
echo "Waiting for connection"
wait
VPN_GW_ADDR=$(awk '/remote IP address/ { print $NF }' ${GW_FILE})

if [ ${VPN_GW_ADDR} ];
then
	for IPADDR in $(host movies.netflix.com | awk '/has address / { print $NF }');
	do
		#echo ip r a ${IPADDR} via ${VPN_GW_ADDR}
		ip r a ${IPADDR} via ${VPN_GW_ADDR}
	done
else
	echo "VPN Connection failed"
	exit 
fi

read -p "Please, digit IP Address from device will connect to netflix: " DEV_IPADDR
while true; 
do
	tcpdump -nn -i any host ${DEV_IPADDR} and not port 33000 and not arp > ${OUTPUT_FILE} 2>/dev/null &
	#tcpdump -nn -i any host ${DEV_IPADDR} and not port 33000 and not arp > ${OUTPUT_FILE} &
	while true;
	do 
		read -p "Please, connect to neflix for sniffing IP Addr and after that digit ready to create routes or no to cancel: " ANSWER
		case ${ANSWER} in
			"ready") killall tcpdump; break;;
			"no") exit 1;;
			*) echo "Please digit ready or no";;
		esac
	done

	if [ -r ${OUTPUT_FILE} ];
	then
		for IPADDR in $(awk '/IP 192.168.0.16/ { print $5 }' ${OUTPUT_FILE} | cut -d . -f1 | sort -u | sed "s/://g");
		do
			#echo ip r a ${IPADDR} via ${VPN_GW_ADDR}
			ip r a "${IPADDR}.0.0.0/8" via ${VPN_GW_ADDR}
		done
		rm ${OUTPUT_FILE}
	else
		echo "Cannot get netflix IPs information"
		exit 1
	fi
done
