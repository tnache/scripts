#!/bin/bash
#

function usage {
	echo "Usage: ./$(basename ${0}) [region] [environment] [server] [root]"
	echo "Region are:"
	echo "- us-east"
	echo "- sa-east"
	echo
	echo "Environment:"
	echo "- prod"
	echo "- dev"
	echo
	echo "root is to run ssh with user root. This is an optional parameter"
	echo
	echo "Server is the short name of server you want to connect"
	exit 1
}

if [ ${3} ];
then
	REGION=${1}
	ENVIRONMENT=${2}
	SERVER=${3}
	if [ ${4} ];
	then
		USER=${4}
	fi

	if [ ${ENVIRONMENT} == "prod" ];
	then
		DOMAINNAME="aws.ig.com.br"
	elif [ ${ENVIRONMENT} == "dev" ];
	then
		DOMAINNAME="dev.aws.ig.com.br"
	else
		echo "Invalid environment"
		usage
	fi

	if [ ${REGION} == "us-east" ] && [ ${ENVIRONMENT} == "prod" ];
	then
                KEYFILE="${HOME}/git/puppet-cloud/ig-aws-us-east.pem"
	elif [ ${REGION} == "us-east" ] && [ ${ENVIRONMENT} == "dev" ];
	then
                KEYFILE="${HOME}/git/puppet-cloud/ig-aws-us-east-dev.pem"
        elif [ ${REGION} =="sa-east" ] && [ ${ENVIRONMENT} == "prod" ];
	then
                KEYFILE="${HOME}/git/puppet-cloud/ig-aws-sa-east.pem"
        elif [ ${REGION} == "sa-east" ] && [ ${ENVIRONMENT} == "dev" ];
	then
                KEYFILE="${HOME}/git/puppet-cloud/ig-aws-sa-east-dev.pem"
        else
		echo "Invalid Region"
		usage
        fi

	LONGNAME="${SERVER}.${DOMAINNAME}"
	if [ ${USER} == "root" ];
	then
		ssh -i ${KEYFILE} -l root ${LONGNAME}
	else
		ssh ${LONGNAME}
	fi
else
	usage
fi
