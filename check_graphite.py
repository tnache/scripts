#!/usr/bin/python

import sys
import baker
import httplib
import memcache

@baker.command()
def get(hostname,object,warning,critical,operator='gt',graphite_port=80):
    mc = memcache.Client(['puppet:11211'], debug=0)
    graphite_addr = mc.get('graphite')
    critical = int(critical)
    warning = int(warning)
    conn = httplib.HTTPConnection(graphite_addr,graphite_port)
    if object == "cpu":
        target = "collectd."+hostname+".cpu-0.cpu-idle"
    elif object == "memory":
        target = "collectd."+hostname+".memory.memory-free"
    else:
        print "Invalid object. Availables:"
        print "-cpu"
        print "-memory"
        sys.exit(3)
    metric = "/render?from=-5minm&target="+target+"&rawData=true"
    conn.request("GET",metric)
    response = conn.getresponse()
    if response.status is 200:
        output = response.read()
    else:
        print "Response code ",response.status
        sys.exit(1)
    output = output.split(',')
    split = output[3].split('|')
    value = float(split[1])
    #value = int(split[1])
    if operator == "gt":
        if value > critical:
            print "CRITICAL: "+metric+" is above "+str(critical)
            print "Value: "+str(value)
            sys.exit(2)
        elif value > warning:
            print "Warning: "+metric+" is above "+str(warning)
            print "Value: "+str(value)
            sys.exit(1)
        else:
            print "OK: "+metric+" is "+str(value)
            sys.exit(0)
    elif operator == "lt":
        if value < critical:
            print "CRITICAL: "+metric+" is above "+str(critical)
            print "Value: "+str(value)
            sys.exit(2)
        elif value < warning:
            print "Warning: "+metric+" is above "+str(warning)
            print "Value: "+str(value)
            sys.exit(1)
        else:
            print "OK: "+metric+" is "+str(value)
            sys.exit(0)
    else:
        print "Invalid operator. Availables:"
        print "gt"
        print "lt"
        sys.exit(3)
baker.run()
