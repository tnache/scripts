#!/usr/bin/perl

#requests examples
# create new frontend
# curl "http://localhost/cgi-bin/haproxycfg.cgi?operation=new&object_type=config&frontend_addr=10.0.2.15&frontend_port=80"
#
# create farm
# curl "http://localhost/cgi-bin/haproxycfg.cgi?farm_name=mynewfarm&backend_name=webserver1&backend_port=80&backend_check=check&operation=new&object_type=config&frontend_addr=10.0.2.15&frontend_port=80"
#modules
use strict;
use CGI;

#variables
my $haproxy_config_file = "/etc/haproxy/haproxy.cfg";
my $global_config = 1;
my @global_config;
my $defaults_config = 1;
my @defaults_config;
my $acl_config = 1;
my @acl_config;
my $use_backend_config = 1;
my @use_backend_config;
my $backend_config = 1;
my @backend_config;
my @new_backend_config;
my $backend_exists = 1;
my $farm_found = 1;
my $farm_exists = 1;
my $frontend;

my $cgi = CGI->new;
print $cgi->header();
print $cgi->start_html();

#data from cgi
my $farm_name = $cgi->param('farm_name');
my $backend_name = $cgi->param('backend_name');
my $backend_port = $cgi->param('backend_port');
my $backend_weight = $cgi->param('backend_weight') || 1;
my $backend_check = $cgi->param('backend_check');
my $operation = $cgi->param('operation');
my $object_type = $cgi->param('object_type');
my $frontend_addr = $cgi->param('frontend_addr');
my $frontend_port = $cgi->param('frontend_port');
my @server_id = split(/\./,$backend_name);

#routines
sub read_config_file {
  open FH,"< $haproxy_config_file" || do { $cgi->h1("Cannot read $haproxy_config_file"); $cgi->end_html; exit 1; }; 
  while ( my $line = <FH> ) {
    chomp($line);
    if ( $line =~ m/^global$/o ) {
      $global_config = 0;
    } elsif ( $line =~ m/^defaults$/o ) {
      $defaults_config = 0;
      $global_config = 1;
    } elsif ( $line =~ m/^frontend/o ) {
      $defaults_config = 1;
      $frontend = $line;
    } elsif ( $line =~ m/^\s+acl/o ) {
      $acl_config = 0;
    } elsif ( $line =~ m/^\s+use_backend/o ) {
      $acl_config = 1;
      $use_backend_config = 0;
    } elsif ( $line =~ m/^\s+backend/o ) {
      $use_backend_config = 1;
      $backend_config = 0;
    }
    if ( $global_config == 0 ) {
      push(@global_config,$line);
    }
    if ( $defaults_config == 0 ) {
      push(@defaults_config,$line);
    }
    if ( $acl_config == 0 ) {
      push(@acl_config,$line);
    }
    if ( $use_backend_config == 0 ) {
      push(@use_backend_config,$line);
    }
    if ( $backend_config == 0 ) {
      push(@backend_config,$line);
    }
  }
  close FH;
}

sub security_check {
  if ( grep(/backend $farm_name/,@backend_config) ) {
    $farm_exists = 0;
  }

  if ( grep(/server @server_id[0] $backend_name:$backend_port/,@backend_config) ) {
    $backend_exists = 0;
  }

  if ( $operation eq 'add' ) {
    if ( $object_type eq 'farm' ) {
      if ( $farm_exists == 0 ) {
        $cgi->h1("Farm $farm_name already exists");
        $cgi->end_html;
        exit 1;
      }
    } elsif ( $object_type eq 'server' ) {
      if ( $farm_exists == 0 ) {
        if ( $backend_exists == 0 ) {
          $cgi->h1("Backend $backend_name already exists");
          $cgi->end_html;
          exit 1;
        }
      } else {
        $cgi->h1("Farm $farm_name not exists");
        $cgi->end_html;
        exit 1;
      }
    }
  } elsif ( $operation eq 'del' ) {
    if ( $object_type eq 'farm' ) {
      if ( $farm_exists == 1 ) {
         $cgi->h1("Farm $farm_name not exists");
         $cgi->end_html;
         exit 1;
      }
    } elsif ( $object_type eq 'server' ) {
      if ( $farm_exists == 0 ) {
        if ( $backend_exists == 1 ) {
          $cgi->h1("Backend $backend_name not exists");
          $cgi->end_html;
          exit 1;
        }
      } else {
        $cgi->h1("Farm $farm_name not exists");
        $cgi->end_html;
        exit 1;
      }
    }
  }
}

sub add_backend {
  foreach my $line ( @backend_config ) {
    if ( $line =~ m/backend $farm_name/o ) {
      $farm_found = 0;
    } elsif ( $line =~ m/backend/o ) {
      $farm_found = 1;
    }
    if ( $farm_found == 0 && $line =~ m/server/o ) {
      my $new_line = "               server @server_id[0] $backend_name:$backend_port weight $backend_weight $backend_check";
      push(@new_backend_config,$new_line);
      $farm_found = 1;
    }
    push(@new_backend_config,$line);
  }
}

sub del_backend {
  foreach my $line ( @backend_config ) {
    if ( $line =~ m/backend $farm_name/o ) {
      $farm_found = 0;
    } elsif ( $line =~ m/backend/o ) {
      $farm_found = 1;
    }
    if ( $line !~ m/server\s@server_id[0]\s$backend_name:$backend_port/o ) {
      push(@new_backend_config,$line);
    }
  }
}

sub create_new_config {
  my $frontend_addr = shift;
  my $frontend_port = shift;

  if ( defined($frontend_port) ) {
    open FH,"> $haproxy_config_file" || do { $cgi->h1("Cannot create config file"); $cgi->end_html; exit 1; };
    print FH "
global
        log 127.0.0.1   local0
        log 127.0.0.1   local1 notice
        #log loghost    local0 info
        ulimit-n 65536
        maxconn 25000
        #debug
        #quiet
        user haproxy
        group haproxy
        stats socket /var/run/haproxy-socket user root level admin
 
defaults
        log     global
        mode    http
        option  httplog
        option  dontlognull
        option  abortonclose
        retries 3
        option  redispatch
        maxconn 25000
        contimeout      300000
        clitimeout      300000
        srvtimeout      3000000
        stats enable
        stats uri     /myhaproxy?stats
        stats auth user:password
 
#IP iniciado pelo heartbeat
frontend all_http $frontend_addr:$frontend_port\n";
    close FH;
  } else {
    $cgi->h1("Missing frontend addr or port");
    $cgi->end_html;
    exit 1;
  }
}

sub print_array {
  my @array = @_;
  foreach my $line ( @array ) {
    print $line."\n";
  }
  print "\n";
}

##if ( defined($operation) && defined($object_type) ) {
if ( defined($farm_name) && defined($backend_name) && defined($backend_port) && defined($backend_weight) && defined($operation) && defined($object_type) && defined($frontend_addr) && defined($frontend_port) ) {
  if ( $object_type eq "config" ) {
    if ( $operation eq "new" ) {
      &create_new_config($frontend_addr,$frontend_port);
    } else {
      $cgi->h1("$operation is invalid operation for config");
      $cgi->end_html;
      exit 1;
    }
  } else {
    &read_config_file;
    &security_check;

    if ( $object_type eq 'server' ) {
      if ( $operation eq 'add' ) {
        &add_backend;
      } elsif ( $operation eq 'del' ) {
        &del_backend;
      } else {
        die "Operation $operation is invalid";
      }
    } elsif ( $object_type eq 'farm' ) {
      die "Not implemented yet";
    } else {
      die "Object type $object_type is invalid"
    }

    &print_array(@global_config);
    print "\n";
    &print_array(@defaults_config);
    print "\n";
    print $frontend."\n";
    print "\n";
    &print_array(@acl_config);
    print "\n";
    &print_array(@use_backend_config);
    print "\n";
    &print_array(@new_backend_config);
	
  } 
  print $cgi->end_html();
  exit 0;
} else {
  print $cgi->h1('Missing arguments');
  print $cgi->end_html();
  exit 1;
}
##} else {

##}