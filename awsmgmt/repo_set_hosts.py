#!/bin/env /usr/bin/python

import httplib
import sys

def usage():
    print "Usage: "+sys.argv[0]+" [instance_name] [domain name]"
    sys.exit(1)

try:
    instance_name = sys.argv[1]
    domain = sys.argv[2]
except:
    usage()

try:
    conn = httplib.HTTPConnection("169.254.169.254")
except Exception as Error:
    print "Cannot connect to 169.254.169.254"
    print "Error: "+str(Error)
    sys.exit(1)

conn.request("GET", "/latest/meta-data/local-ipv4")
response = conn.getresponse()
if response.status is 200:
    ip_addr = response.read().lower()
else:
   print "Response code ",response.status
   sys.exit(1)

try:
    fh = open('/etc/hosts','a')
except Exception as Error:
    print "Cannot open file /etc/hosts"
    print "Error: "+str(Error)
    sys.exit(1)

fh.write(ip_addr+'\t'+instance_name+'.'+domain+'\t'+instance_name+'\trepo\tmyrepo\n')
fh.close()
