#!/bin/env /usr/bin/python
#
import baker
import boto
from boto.ec2.autoscale import AutoScaleConnection

@baker.command
def list_lc(zone,debug=False):
    if debug is True: print "Connecting to zone "+zone
    conn_autoscale = boto.ec2.autoscale.connect_to_region(zone)
    list = conn_autoscale.get_all_launch_configurations()
    for item in list:
        print item

@baker.command
def list_inst(zone,debug=False):
    if debug is True: print "Connecting to zone "+zone
    conn_autoscale = boto.ec2.autoscale.connect_to_region(zone)
    list = conn_autoscale.get_all_autoscaling_instances()
    for item in list:
        print item

@baker.command
def list_activities(zone,groupname,debug=False):
    if debug is True: print "Connecting to zone "+zone
    conn_autoscale = boto.ec2.autoscale.connect_to_region(zone)
    list = conn_autoscale.get_all_activities(groupname)
    for item in list:
        print item

@baker.command
def list_policies(zone,debug=False):
    if debug is True: print "Connecting to zone "+zone
    conn_autoscale = boto.ec2.autoscale.connect_to_region(zone)
    list = conn_autoscale.get_all_policies()
    for item in list:
        print item

@baker.command
def delete_lc(zone,lcname,debug=False):
    if debug is True: print "Connecting to zone "+zone
    conn_autoscale = boto.ec2.autoscale.connect_to_region(zone)
    conn_autoscale.delete_launch_configuration(lcname)

@baker.command
def delete_asg(zone,asgname,debug=False):
    if debug is True: print "Connecting to zone "+zone
    conn_autoscale = boto.ec2.autoscale.connect_to_region(zone)
    conn_autoscale.delete_auto_scaling_group(asgname)

@baker.command
def delete_policy(zone,policyname,groupname,debug=False):
    if debug is True: print "Connecting to zone "+zone
    conn_autoscale = boto.ec2.autoscale.connect_to_region(zone)
    conn_autoscale.delete_policy(policyname,groupname)

baker.run()
