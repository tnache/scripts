#!/bin/env /usr/bin/python

import httplib
import sys

def usage():
    print "Usage: "+sys.argv[0]+" [puppet address]"
    sys.exit(1)
try:
    puppet_addr = sys.argv[1]
except:
    usage()

try:
    fh = open('/etc/hosts','a')
except Exception as Error:
    print "Cannot open file /etc/hosts"
    print "Error: "+str(Error)
    sys.exit(1)

fh.write(puppet_addr+'\t puppet\n')
fh.close()
