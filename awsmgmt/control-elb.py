#!/usr/bin/python
#
import sys
import time
import baker
import boto.ec2
import boto.ec2.elb
from boto.ec2.elb import HealthCheck

@baker.command
#def create(zone,lbname,interval,threshold_health,threshold_unhealth,target,ports,key,type,ami,securitygroup):
def create(zone,lbname,interval,threshold_health,threshold_unhealth,target,availabilityzone):
    "Create new LB"
    ports=[(80, 8080, 'http'), (443, 9443, 'tcp')]
    #availability_zone=['us-east-1a', 'us-east-1b']
    availabilityzone = availabilityzone.split(',')
    #print "ZONE: ",zone
    conn = boto.ec2.elb.connect_to_region(zone)
    #print conn
    hc = HealthCheck(
        interval=interval,
        healthy_threshold=threshold_health,
        unhealthy_threshold=threshold_unhealth,
        target=target
    )
    #print hc
    lb = conn.create_load_balancer(lbname,availabilityzone,ports)
    lb.configure_health_check(hc)
    print lb.dns_name
    sys.exit(0)

@baker.command
def register(zone,lbdnsname,instance_id):
    "Add new instance to elb"
    conn_elb = boto.ec2.elb.connect_to_region(zone)
    conn_elb.register_instances(lbdnsname,instance_id)

@baker.command
def get(zone):
    "List all elb"
    conn_elb = boto.ec2.elb.connect_to_region(zone)
    lbs = conn_elb.get_all_load_balancers()
    print lbs

baker.run()
