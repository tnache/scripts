#!/bin/bash

mysqladmin -u root password 'password'
mysqladmin -u root -p'password' create puppet
mysql -u root -p'password' -e "CREATE USER 'puppet'@'localhost'  IDENTIFIED BY 'puppet';"
mysql -u root -p'password' -e "CREATE USER 'puppet'@'%' IDENTIFIED BY 'puppet';"
mysql -u root -p'password' -e "GRANT ALL PRIVILEGES ON puppet.* TO 'puppet'@'localhost';"
mysql -u root -p'password' -e "GRANT ALL PRIVILEGES ON puppet.* TO 'puppet'@'%';"
mysqladmin -u root -p'password' flush-privileges
service mysqld restart
