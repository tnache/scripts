#!/usr/bin/env /usr/bin/python

import sys
import memcache
import httplib

memcached_addr = sys.argv[1]
memcached_port = sys.argv[2]

conn = httplib.HTTPConnection("169.254.169.254")
conn.request("GET", "/latest/meta-data/local-ipv4")
response = conn.getresponse()
if response.status is 200:
    ip_addr = response.read().lower()
else:
   print "Response code ",response.status
   sys.exit(1)


mc = memcache.Client([memcached_addr+':'+memcached_port], debug=0)
mc.set('puppetmaster',ip_addr)
