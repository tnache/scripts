#!/bin/env /usr/bin/python
#
#
import sys
import baker
from boto.route53.connection import Route53Connection
from boto.route53.record import ResourceRecordSets

@baker.command
def add(domain,name,content,type="CNAME",ttl=300,debug=False):
    domain = domain+'.'
    conn_route53 = Route53Connection()
    name = name+'.'+domain
    
    results = conn_route53.get_all_hosted_zones()
    zones = results['ListHostedZonesResponse']['HostedZones']
    found = 0
    for zone in zones:
        if zone['Name'] == domain:
            found = 1
            zone_id = zone['Id'].replace('/hostedzone/', '')
            if debug is True: print "ID: "+zone_id+" N: "+name+" T: "+str(ttl)
            changes = ResourceRecordSets(conn_route53, zone_id)
            change = changes.add_change("CREATE",name,type,ttl)
            change.add_value(content)
            changes.commit()
            break
    if not found:
        print "No Route53 zone found for %s" % domain

baker.run()
