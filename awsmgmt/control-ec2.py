#!/bin/env /usr/bin/python
#
import sys, time, baker, boto.ec2, paramiko, os, memcache

@baker.command
def monitor(zone):
    "Monitor Control VMs"
    conn_ec2 = boto.ec2.connect_to_region(zone)
    reservations = conn_ec2.get_all_instances()
    print reservations

@baker.command
def get(zone,debug=False):
    "Get all instances"
    conn_ec2 = boto.ec2.connect_to_region(zone)
    reservations = conn_ec2.get_all_instances()
    if debug is True: print "Reservations: ",reservations
    for reservation in reservations:
        instances = reservation.instances
        if debug is True: print "Instance: ",instances
        for instance in instances:
            try:
                instance_name = instance.tags['Name']
            except:
                instance_name = "Undef"
            print instance.id,instance_name,instance._state,instance.public_dns_name,instance.private_dns_name
            #print instance.id,instance._state,instance.public_dns_name,instance.private_dns_name

@baker.command
def terminate(zone,instance_id):
    "Terminate a instance"
    conn_ec2 = boto.ec2.connect_to_region(zone)
    terminate = conn_ec2.terminate_instances(instance_ids=[instance_id])
    print terminate

@baker.command
def stop(zone,instance_id):
    "Terminate a instance"
    conn_ec2 = boto.ec2.connect_to_region(zone)
    stop = conn_ec2.stop_instances(instance_ids=[instance_id])
    print stop

@baker.command
def start(zone,instance_id):
    "Terminate a instance"
    conn_ec2 = boto.ec2.connect_to_region(zone)
    start = conn_ec2.start_instances(instance_ids=[instance_id])
    print start

baker.run()
