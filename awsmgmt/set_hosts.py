#!/bin/env /usr/bin/python

import httplib
import sys
from subprocess import call

instance_name = sys.argv[1]
domain = sys.argv[2]

conn = httplib.HTTPConnection("169.254.169.254")
conn.request("GET", "/latest/meta-data/local-ipv4")
response = conn.getresponse()
if response.status is 200:
    ip_addr = response.read().lower()
else:
   print "Response code ",response.status
   sys.exit(1)


fh = open('/etc/hosts','a')
fh.write(ip_addr+'\t'+instance_name+'.'+domain+'\t'+instance_name+'\n')
fh.close()

call(["hostname",instance_name])
