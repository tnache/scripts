#!/bin/env /usr/bin/python

import httplib
import sys

instance_name = sys.argv[1]
puppet_addr = sys.argv[2]
repo_addr = sys.argv[3]
graphite_addr = sys.argv[4]
nagios_addr = sys.argv[5]

conn = httplib.HTTPConnection("169.254.169.254")
conn.request("GET", "/latest/meta-data/local-ipv4")
response = conn.getresponse()
if response.status is 200:
    ip_addr = response.read().lower()
else:
   print "Response code ",response.status
   sys.exit(1)

fh = open('/etc/hosts','w')
fh.write('127.0.0.1\tlocalhost.localdomain localhost\n')
fh.write(ip_addr+'\t'+instance_name+'\n')
fh.write(graphite_addr+'\t graphite\n')
fh.write(puppet_addr+'\t puppet\n')
fh.write(repo_addr+'\t repo\n')
fh.write(nagios_addr+'\t nagios\n')
fh.close()
