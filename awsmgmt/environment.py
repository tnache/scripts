#!/bin/env /usr/bin/python
#
import sys
import os
import time
import baker
import boto.ec2
import paramiko
import memcache
import boto.ec2.elb
from boto.ec2.elb import HealthCheck
from boto.route53.connection import Route53Connection
from boto.route53.record import ResourceRecordSets
from boto.ec2.autoscale import AutoScaleConnection
from boto.ec2.autoscale import LaunchConfiguration
from boto.ec2.autoscale import AutoScalingGroup
from boto.ec2.autoscale import ScalingPolicy
import boto.ec2.cloudwatch
from boto.ec2.cloudwatch import MetricAlarm

@baker.command
def create_instance(debug,zone,key,type,ami,securitygroups,tag,name,domain,username,ssh_port,keyfile,cmds,known_hosts=os.getenv("HOME")+"/.ssh/known_hosts"):
    "Create a new instance"
    print "Deploying a new instance"
    # verificar image = c.get_image(ec2_ami)
    securitygroups = securitygroups.split(',')
    if debug is True: print "Connection to region "+zone
    try:
        conn_ec2 = boto.ec2.connect_to_region(zone)
    except Exception as Error:
        print "Cannot connect to api"
        print "Error: "+str(Error)
        sys.exit(1)
    if debug is True: print "Deploying instance"
    try:
        reservation = conn_ec2.run_instances(
             ami,
             key_name=key,
             instance_type=type,
             security_groups=securitygroups)
    except Exception as Error:
        print "Reservation error"
        print "Error: "+str(Error)
        sys.exit(1)
    instance = reservation.instances[0]
    status = instance.update()
    if debug is True: print "Waiting for instance to be created"
    count = 0
    while status == 'pending':
        count = count + 1
        if debug is True: print "Sleeping 10 seconds"
        time.sleep(10)
        status = instance.update()
        if count >= 20:
            print "Max retries exceeded"
            sys.exit(1)
    if status == 'running':
        tag = tag.split(',')
        if debug is True: print "Adding tag Name"
        conn_ec2.create_tags([instance.id],{"Name": name+"."+domain})
        for tagcurrent in tag:
            tagcurrent = tagcurrent.split('=')
            if debug is True: print "Adding tag "+tagcurrent[0]
            instance.add_tag(tagcurrent[0],tagcurrent[1])
    else:
        print('Instance status: ' + status)
        return None
    if debug is True: print "Waiting for installation "
    count = 0
    condition = False
    while condition is False:
        count = count + 1
        try:
            client = paramiko.SSHClient()
            client.save_host_keys(known_hosts)
            client.load_host_keys(known_hosts)
            client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            client.connect(instance.public_dns_name,port=ssh_port,username=username,key_filename=keyfile)
            condition = True
        except:
            if debug is True: print "Sleeping 10 seconds"
            time.sleep(10)
        if count >= 20:
            print "Max retries exceeded"
            sys.exit(1)
    if debug is True: print "Adding DNS Record"
    try:
        route53_add_record(domain,name,instance.public_dns_name)
    except Exception as Error:
        print "Cannot add DNS record"
        print "Error: "+str(Error)
    return instance

@baker.command
def deploy(zone,type,ami,tag,name,domain,lbname=None,interval=None,threshold_health=None,threshold_unhealth=None,target=None,availabilityzone=None,username="ec2-user",ssh_port="22",known_hosts=os.getenv("HOME")+"/.ssh/known_hosts",key="ssh",keyfile=os.getenv("HOME")+"/.ssh/ssh.pem",securitygroups="default",debug=False):
    "Create a new environment"
    print "Name: "+name
    origin_path = '/home/tnache/awsmgmt'
    graphites = getservers(zone,'graphite','private_ip_address')
    graphite_addr = graphites[0]
    scripts = ["set_hosts.py",
               "set_rc_local.py",]

    cmds = ["sudo /tmp/set_hosts.py "+name+" "+domain,
            "sudo /tmp/set_rc_local.py "+name,]

    if name == "puppetmaster-a":
        puppet_addr = "127.0.0.1"
        scripts = scripts+["puppet_set_hosts.py",
                   "puppetmaster_node_classifier.py",
                   "puppetmaster_git_download.sh",
                   "puppetmaster_set_memcached.py",
                   "puppetmaster_mysql.sh"]
        package = 'python-memcached.noarch memcached puppet git rubygems ruby-devel ruby-mysql mysql-server'
        cmds = cmds+["sudo /tmp/puppet_set_hosts.py "+puppet_addr,
                "sudo yum install -y "+package+" puppet-server",
                "sudo bash /tmp/puppetmaster_git_download.sh",
                "sudo chmod +x /tmp/puppetmaster_node_classifier.py",
                "sudo gem install activerecord --version 2.3.5",
                "sudo /sbin/chkconfig mysqld on",
                "sudo /sbin/service mysqld start",
                "sudo bash /tmp/puppetmaster_mysql.sh",
                "sudo /sbin/chkconfig puppetmaster on",
                "sudo /sbin/service puppetmaster start"]
    elif name == "repo-a":
	puppets = getservers(zone,'puppetmaster','private_ip_address')
        puppet_addr = puppets[0]
        scripts = scripts+["puppet_set_hosts.py",
                           'repo_set_hosts.py',
                           'puppet.conf']
        package = 'puppet'
        cmds = cmds+["sudo /tmp/puppet_set_hosts.py "+puppet_addr,
                "sudo yum install -y "+package,
                "sudo /tmp/repo_set_hosts.py "+name+" "+domain,
                "sudo cp /tmp/puppet.conf /etc/puppet/puppet.conf"]
    elif name == "nagios-a":
        puppets = getservers(zone,'puppetmaster','private_ip_address')
        puppet_addr = puppets[0]
        repos = getservers(zone,'repository','private_ip_address')
        repo_addr = repos[0]
        scripts = scripts+["puppet_set_hosts.py",
                           'puppet_node_notify.py',
                           'puppet.conf',]
        package = 'puppet'
        cmds = cmds+["sudo /tmp/puppet_set_hosts.py "+puppet_addr,
                     "sudo yum install -y "+package,
                "sudo /tmp/puppet_node_notify.py "+puppet_addr+" "+ repo_addr+" "+graphite_addr,
                "sudo cp /tmp/puppet.conf /etc/puppet/puppet.conf",]
    else:
        puppets = getservers(zone,'puppetmaster','private_ip_address')
        puppet_addr = puppets[0]
        repos = getservers(zone,'repository','private_ip_address')
        repo_addr = repos[0]
        scripts = scripts+["puppet_set_hosts.py",
                           'puppet_node_notify.py',
                           'puppet.conf']
        package = 'puppet'
        cmds = cmds+["sudo /tmp/puppet_set_hosts.py "+puppet_addr,
                     "sudo yum install -y "+package,
                "sudo /tmp/puppet_node_notify.py "+puppet_addr+" "+ repo_addr+" "+graphite_addr,
                "sudo cp /tmp/puppet.conf /etc/puppet/puppet.conf"]
    
    cmds = cmds+["sudo puppet agent --onetime --no-daemonize",
                "sudo puppet agent --onetime --no-daemonize",
                "sudo puppet agent --onetime --no-daemonize",
                "sudo yum update -y",
                "sudo reboot"]

    instance = create_instance(debug,zone,key,type,ami,securitygroups,tag,name,domain,username,ssh_port,keyfile,cmds,known_hosts)
    bootstrap(debug,scripts,keyfile,username,ssh_port,cmds,instance)
    print "Instance: "+instance.public_dns_name,instance.private_dns_name
    if lbname is not None:
        lb = create_elb(zone,availabilityzone,lbname,interval,threshold_health,threshold_unhealth,target)
        instance_id = str(instance.id)
        register_instance(zone,lbname,instance_id)
        print "LB: "+lb.dns_name

@baker.command
def create_elb(zone,availabilityzone,lbname,domain,interval,threshold_health,threshold_unhealth,target,debug=False):
    "Create a new Elastic Load Balancer"
    ports=[(80, 80, 'http'), (443, 443, 'tcp')]
    availabilityzone = availabilityzone.split(',')
    try:
        if debug is True: print "Connecting to "+zone
        conn_elb = boto.ec2.elb.connect_to_region(zone)
    except Exception as Error:
        print "Connection error"
        print "Error: "+str(Error)
        sys.exit(1)

    try:
        if debug is True: print "Creating ELB"
        lb = conn_elb.create_load_balancer(lbname,availabilityzone,ports)
    except Exception as Error:
        print "Cannot create ELB"
        print "Error: "+str(Error)
        sys.exit(1)
    try:
        if debug is True: print "Configuring healthcheck"
        hc = HealthCheck(
            interval=interval,
            healthy_threshold=threshold_health,
            unhealthy_threshold=threshold_unhealth,
            target=target
        )
        lb.configure_health_check(hc)
    except Exception as Error:
        print "Cannot configure healthcheck ELB"
        print "Error: "+str(Error)
        sys.exit(1)

    try:
        if debug is True: print "Adding DNS record"
        route53_add_record(domain,lbname,lb.dns_name)
    except Exception as Error:
        print "Cannot add to DNS"
        print "Error: "+str(Error)

    return lb

@baker.command
def register_instance(zone,lbname,instance_id,debug=False):
    "Register instance on ELB"
    conn_elb = boto.ec2.elb.connect_to_region(zone)
    conn_elb.register_instances(lbname,instance_id)

def bootstrap(debug,scripts,keyfile,username,port,cmds,instance):
    "Run bootstrap"
    print "Starting bootstrap"
    for script in scripts:
        scp_cmd = "scp -i "+keyfile+" "+script+" "+username+"@"+instance.public_dns_name+":/tmp/"+script
        output = os.system(scp_cmd)

    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    client.connect(instance.public_dns_name,port=port,username=username,key_filename=keyfile)

    for cmd in cmds:
        chan = client.get_transport().open_session()
        chan.get_pty()
        if debug is True: print "Running command: "+cmd
        file = chan.makefile()
        chan.exec_command(cmd)
        while chan.recv_ready is False:
            print "sleep"
            sys.sleep(5)
        print file.read(),

def route53_add_record(domain,name,content,type="CNAME",ttl=300,debug=False):
    domain = domain+'.'
    conn_route53 = Route53Connection()
    name = name+'.'+domain
    
    results = conn_route53.get_all_hosted_zones()
    zones = results['ListHostedZonesResponse']['HostedZones']
    found = 0
    for zone in zones:
        if zone['Name'] == domain:
            found = 1
            zone_id = zone['Id'].replace('/hostedzone/', '')
            if debug is True: print "ID: "+zone_id+" N: "+name+" T: "+str(ttl)
            changes = ResourceRecordSets(conn_route53, zone_id)
            change = changes.add_change("CREATE",name,type,ttl)
            change.add_value(content)
            changes.commit()
            break
    if not found:
        print "No Route53 zone found for %s" % domain

@baker.command
def getservers(zone,function,attribute,debug=False):
    "Get servers in function"
    ips = None
    conn_ec2 = boto.ec2.connect_to_region(zone)
    reservations = conn_ec2.get_all_instances()
    if debug is True: print "Reservations: ",reservations
    for reservation in reservations:
        instances = reservation.instances
        if debug is True: print "Instances: ",instances
        for instance in instances:
            try:
                function_instance = str(instance.tags['function'])
                state = str(instance._state)
                if debug is True: print "function: "+function_instance+" state: "+state
                if function_instance == function and state == "running(16)":
                    if ips is None:
                        if attribute == "public_dns_name":
                            ips = [instance.public_dns_name]
                        elif attribute == "private_ip_address":
                            ips = [instance.private_ip_address]
                    else:
                        if attribute == "public_dns_name":
                            ips = ips + [instance.public_dns_name]
                        elif attribute == "private_ip_address":
                            ips = ips + [instance.private_ip_address]
            except:
                print "Cannot determine function. Skipping"
    return ips

@baker.command
def terminate(zone,instance_id,name,known_hosts=os.getenv("HOME")+"/.ssh/known_hosts",username="ec2-user",port=22,keyfile=os.getenv("HOME")+"/.ssh/ssh.pem"):
    "Terminate a instance"
    print "Terminating instance"
    conn_ec2 = boto.ec2.connect_to_region(zone)
    terminate = conn_ec2.terminate_instances(instance_ids=[instance_id])
    print terminate
    print "Removing from puppet"
    client = paramiko.SSHClient()
    client.save_host_keys(known_hosts)
    client.load_host_keys(known_hosts)
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    puppets = getservers(zone,'puppetmaster','public_dns_name')
    puppet_addr = puppets[0]
    client.connect(puppet_addr,port=port,username=username,key_filename=keyfile)
    condition = True
    chan = client.get_transport().open_session()
    chan.get_pty()
    chan.exec_command("sudo puppet node clean "+name)
    while chan.recv_ready is False:
        time.sleep(5)
    print chan.recv(20480)
    print chan.recv_stderr(20480)

@baker.command
def set_hosts(zone,known_hosts=os.getenv("HOME")+"/.ssh/known_hosts",username="ec2-user",ssh_port=22,keyfile=os.getenv("HOME")+"/.ssh/ssh.pem",debug=False):
    "Create a new host file all instances"
    graphites = getservers(zone,'graphite','private_ip_address')
    graphite_addr = str(graphites[0])
    
    puppets = getservers(zone,'puppetmaster','private_ip_address')
    puppet_addr = str(puppets[0])
    
    repos = getservers(zone,'repository','private_ip_address')
    repo_addr = str(repos[0])
    
    nagios = getservers(zone,'nagios','private_ip_address')
    nagios_addr = str(nagios[0])

    conn_ec2 = boto.ec2.connect_to_region(zone)
    reservations = conn_ec2.get_all_instances()
    if debug is True: print "Reservations: ",reservations
    for reservation in reservations:
        instances = reservation.instances
        if debug is True: print "Instances: ",instances
        for instance in instances:
            state = str(instance._state)
            if debug is True: print "State: "+state
            if state == "running(16)":
                scp_cmd = "scp -i "+keyfile+" set_all_hosts.py"+" "+username+"@"+instance.public_dns_name+":/tmp/"
                output = os.system(scp_cmd)
                client = paramiko.SSHClient()
                client.save_host_keys(known_hosts)
                client.load_host_keys(known_hosts)
                client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
                client.connect(instance.public_dns_name,port=ssh_port,username=username,key_filename=keyfile)
                chan = client.get_transport().open_session()
                chan.get_pty()
                instance_name = str(instance.tags['Name'])
                cmd = 'sudo python /tmp/set_all_hosts.py '+instance_name+' '+puppet_addr+' '+repo_addr+' '+graphite_addr+' '+nagios_addr
                if debug is True: print "Running command: "+cmd
                chan.exec_command(str(cmd))
                while chan.recv_ready is False:
                    time.sleep(5)
                print chan.recv(20480)
                print chan.recv_stderr(20480)

@baker.command
def create_metric_alarm(zone,groupname,metric,statistic,threshold,period,evaluationperiods,debug=False):
    "Create metric alarm"
    try:
        if debug is True: print "Connecting to autoscale"
        conn_autoscale = boto.ec2.autoscale.connect_to_region(zone)
    except Exception as Error:
        print "Cannot connect"
        print "Error: "+str(Error)
        sys.exit(1)

    scale_up_policy = ScalingPolicy(
            name='scale_up', adjustment_type='ChangeInCapacity',
            as_name=groupname, scaling_adjustment=1, cooldown=180)
    scale_down_policy = ScalingPolicy(
            name='scale_down', adjustment_type='ChangeInCapacity',
            as_name=groupname, scaling_adjustment=-1, cooldown=180)
    try:
        if debug is True: print "Creating policy"
        conn_autoscale.create_scaling_policy(scale_up_policy)
        conn_autoscale.create_scaling_policy(scale_down_policy)
    except Exception as Error:
        print "Cannot create scale policy"
        print "Error: "+str(Error)
        sys.exit(1) 
    scale_up_policy = conn_autoscale.get_all_policies(
            as_group=groupname, policy_names=['scale_up'])[0]
    scale_down_policy = conn_autoscale.get_all_policies(
            as_group=groupname, policy_names=['scale_down'])[0]
    try:
        if debug is True: print "Connecting to cloudwatch"
        conn_cloudwatch = boto.ec2.cloudwatch.connect_to_region(zone)
    except Exception as Error:
        print "Cannot connect"
        print "Error: "+str(Error)
        sys.exit(1)

    alarm_dimensions = {"AutoScalingGroupName": groupname}
    
    scale_alarm_up = MetricAlarm(
            name='scale_up_on_cpu', namespace='AWS/EC2',
            metric=metric, statistic=statistic,
            comparison='>', threshold=threshold,
            period=period, evaluation_periods=evaluationperiods,
            alarm_actions=[scale_up_policy.policy_arn],
            dimensions=alarm_dimensions)
    try:
        if debug is True: print "Creating up alarm"
        conn_cloudwatch.create_alarm(scale_alarm_up)
    except Exception as Error:
        print "Cannot create metric alarm"
        print "Error: "+str(Error)
        sys.exit(1)

    scale_alarm_down = MetricAlarm(
            name='scale_down_on_cpu', namespace='AWS/EC2',
            metric=metric, statistic=statistic,
            comparison='<', threshold=threshold,
            period=period, evaluation_periods=evaluationperiods,
            alarm_actions=[scale_down_policy.policy_arn],
            dimensions=alarm_dimensions)
    try:
        if debug is True: print "Creating down alarm"
        conn_cloudwatch.create_alarm(scale_alarm_down)
    except Exception as Error:
        print "Cannot create metric alarm"
        print "Error: "+str(Error)
        sys.exit(1)

@baker.command
def create_autoscaling(zone,lc_name,ami,groupname,lbname,minsize,maxsize,availabilityzones=['us-east-1a','us-east-1b'],securitygroups="default",keyname="ssh",debug=False):
    "Create launch configuration for AutoScaling"
    try:
        conn_autoscale = boto.ec2.autoscale.connect_to_region(zone)
    except Exception as Error:
        print "Cannot connect to api"
        print "Error: "+str(Error)
        sys.exit(1)
    securitygroups = securitygroups.split(',')
    lc = LaunchConfiguration(name=lc_name, image_id=ami,
                             key_name=keyname,
                             security_groups=securitygroups)
    conn_autoscale.create_launch_configuration(lc)
    ag = AutoScalingGroup(group_name=groupname,load_balancers=[lbname],
                          availability_zones=availabilityzones,
                          launch_config=lc, min_size=minsize, max_size=maxsize,
                          connection=conn_autoscale)
    try:
        conn_autoscale.create_auto_scaling_group(ag)
        ag.get_activities()
    except Exception as Error:
        print "Cannot create auto scaling group"
        print "Error: "+str(Error)
        sys.exit(1)

baker.run()
