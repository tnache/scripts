#!/bin/env /usr/bin/python

import sys

def usage():
    print "Usage: "+sys.argv[0]+" [puppetmaster_addr] [repository_addr] [graphite_addr]"
    sys.exit(1)

try:
    puppetmaster = sys.argv[1]
    repository = sys.argv[2]
    graphite = sys.argv[3]
except:
    usage()

try:
    fh = open('/etc/hosts','a')
except Exception as Error:
    print "Cannot open file /etc/hosts"
    print "Error: "+str(Error)
    sys.exit(1)

fh.write(puppetmaster+'\tpuppet\n')
fh.write(repository+'\trepository myrepo repo\n')
fh.write(graphite+'\tgraphite\n')
fh.close()
