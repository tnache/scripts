#!/bin/env /usr/bin/python

import sys

def usage():
    print "Usage: "+sys.argv[0]+" [instance_name]"
    sys.exit(1)

try:
    instance_name = sys.argv[1]
except:
    usage()

try:
    fh = open('/etc/rc.local','a')
except Exception as Error:
    print "Cannot open file /etc/rc.local:"
    print "Error: "+str(Error)
    sys.exit(1)

fh.write('hostname '+instance_name+'\n');
fh.write('puppet agent --onetime --no-daemonize --verbose\n')
fh.close()
