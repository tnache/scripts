#!/usr/bin/perl
#

use strict;

my $haproxy_config_file = "/etc/haproxy/haproxy.cfg";
my $global_config = 1;
my @global_config;
my $defaults_config = 1;
my @defaults_config;
my $acl_config = 1;
my @acl_config;
my $use_backend_config = 1;
my @use_backend_config;
my $backend_config = 1;
my @backend_config;
my @new_backend_config;
my $backend_exists = 1;
my $farm_found = 1;
my $farm_exists = 1;
my $frontend;

#vem do cgi
my $farm_name = 'backlot_api_farm';
my $backend_name = 'backlot-api-12.ig.infra';
#my $backend_name = 'backlot-api-13.ig.infra';
my $backend_port = '80';
my $backend_weight = 1;
my $backend_check = 'check';
#my $operation = 'add';
#my $operation = 'del';
my $operation = 'new';
#my $object_type = 'server';
my $object_type = 'config';
#my $object_type = 'farm';
my $frontend_addr = '10.100.64.19';
my $frontend_port = '80';
my @server_id = split(/\./,$backend_name);

sub read_config_file {
  open FH,"< $haproxy_config_file" || die "Cannot read $haproxy_config_file";
  while ( my $line = <FH> ) {
    chomp($line);
    if ( $line =~ m/^global$/o ) {
      $global_config = 0;
    } elsif ( $line =~ m/^defaults$/o ) {
      $defaults_config = 0;
      $global_config = 1;
    } elsif ( $line =~ m/^frontend/o ) {
      $defaults_config = 1;
      $frontend = $line;
    } elsif ( $line =~ m/^\s+acl/o ) {
      $acl_config = 0;
    } elsif ( $line =~ m/^\s+use_backend/o ) {
      $acl_config = 1;
      $use_backend_config = 0;
    } elsif ( $line =~ m/^\s+backend/o ) {
      $use_backend_config = 1;
      $backend_config = 0;
    }
    if ( $global_config == 0 ) {
      push(@global_config,$line);
    }
    if ( $defaults_config == 0 ) {
      push(@defaults_config,$line);
    }
    if ( $acl_config == 0 ) {
      push(@acl_config,$line);
    }
    if ( $use_backend_config == 0 ) {
      push(@use_backend_config,$line);
    }
    if ( $backend_config == 0 ) {
      push(@backend_config,$line);
    }
  #print $line."\n";
  }
  close FH;
}

sub security_check {
  if ( grep(/backend $farm_name/,@backend_config) ) {
    $farm_exists = 0;
  }

  if ( grep(/server @server_id[0] $backend_name:$backend_port/,@backend_config) ) {
    $backend_exists = 0;
  }

  if ( $operation eq 'add' ) {
    if ( $object_type eq 'farm' ) {
      if ( $farm_exists == 0 ) {
        die "Farm $farm_name already exists";
      }
    } elsif ( $object_type eq 'server' ) {
      if ( $farm_exists == 0 ) {
        if ( $backend_exists == 0 ) {
          die "Backend $backend_name already exists";
        }
      } else {
        die "Farm $farm_name not exists";
      }
    }
  } elsif ( $operation eq 'del' ) {
    if ( $object_type eq 'farm' ) {
      if ( $farm_exists == 1 ) {
         die "Farm $farm_name not exists";
      }
    } elsif ( $object_type eq 'server' ) {
      if ( $farm_exists == 0 ) {
        if ( $backend_exists == 1 ) {
          die "Backend $backend_name not exists";
        }
      } else {
        die "Farm $farm_name not exists";
      }
    }
  }
}

sub add_backend {
  foreach my $line ( @backend_config ) {
    if ( $line =~ m/backend $farm_name/o ) {
      $farm_found = 0;
    } elsif ( $line =~ m/backend/o ) {
      $farm_found = 1;
    }
    if ( $farm_found == 0 && $line =~ m/server/o ) {
      my $new_line = "               server @server_id[0] $backend_name:$backend_port weight $backend_weight $backend_check";
      push(@new_backend_config,$new_line);
      $farm_found = 1;
    }
    push(@new_backend_config,$line);
  }
}

sub del_backend {
  foreach my $line ( @backend_config ) {
    if ( $line =~ m/backend $farm_name/o ) {
      $farm_found = 0;
    } elsif ( $line =~ m/backend/o ) {
      $farm_found = 1;
    }
    if ( $line !~ m/server\s@server_id[0]\s$backend_name:$backend_port/o ) {
      push(@new_backend_config,$line);
    }
  }
}

sub create_new_config {
  my $frontend_addr = shift;
  my $frontend_port = shift;

  if ( defined($frontend_port) ) {
    open FH,"> $haproxy_config_file" || die "Cannot create config file";
    print FH "
global
        log 127.0.0.1   local0
        log 127.0.0.1   local1 notice
        #log loghost    local0 info
        ulimit-n 65536
        maxconn 25000
        #debug
        #quiet
        user haproxy
        group haproxy
        stats socket /var/run/haproxy-socket user root level admin
 
defaults
        log     global
        mode    http
        option  httplog
        option  dontlognull
        option  abortonclose
        retries 3
        option  redispatch
        maxconn 25000
        contimeout      300000
        clitimeout      300000
        srvtimeout      3000000
        stats enable
        stats uri     /myhaproxy?stats
        stats auth admin:g3n3s!s
 
#IP iniciado pelo heartbeat
frontend all_http $frontend_addr:$frontend_port\n";
    close FH;
  } else {
    die "Missing frontend addr or port";
  }
}

if ( $object_type eq "config" ) {
  if ( $operation eq "new" ) {
    &create_new_config($frontend_addr,$frontend_port);
  } else {
    die "$operation is invalid operation for config";
  }
} else {
  &read_config_file;
  &security_check;

  if ( $object_type eq 'server' ) {
    if ( $operation eq 'add' ) {
      &add_backend;
    } elsif ( $operation eq 'del' ) {
      &del_backend;
    } else {
      die "Operation $operation is invalid";
    }
  } elsif ( $object_type eq 'farm' ) {
    die "Not implemented yet";
  } else {
    die "Object type $object_type is invalid"
  }

  #print "@global_config\n\n\n";
  foreach my $line ( @global_config ) {
    print $line."\n";
  }
  print "\n";
  #print "@defaults_config\n\n\n";
  foreach my $line ( @defaults_config ) {
    print $line."\n";
  }
  print "\n";
  print $frontend."\n";;
  print "\n";
  #print "@acl_config\n\n\n";
  foreach my $line ( @acl_config ) {
    print $line."\n";
  }
  print "\n";
  #print "@use_backend_config\n\n\n";
  foreach my $line ( @use_backend_config ) {
    print $line."\n";
  }
  print "\n";
  
  foreach my $line ( @new_backend_config ) {
    print $line."\n";
  }
} 
exit 0;