#!/usr/bin/python

import sys, json

output_file = open('probes.json').read()

output_json = json.loads(output_file)

for dict in output_json:
  #print dict
  for a in dict:
    #print a
    if a == "probe_dest":
      URL = dict[a].replace('http://','')
      URL.replace('https://','')
      SPLITED = URL.split('/')
    elif a == "probe_data":
      DATA = dict[a]
    elif a == "checkcontents":
      CHECKCONTENT = dict[a]
    elif a == "contentmatch":
      CONTENTMATCH = dict[a]
    elif a == "probe_desc":
      DESC = dict[a].replace(' ','_')

  SERVERNAME = SPLITED.pop(0)

  try:
     SPLITED[0]
     for ITEM in SPLITED:
       try:
         URI = URI+'/'+ITEM
       except:
         URI = '/'+ITEM
  except:
    URI = '/'
  
  try:
    CONTENTMATCH[0]
  except:
    CONTENTMATCH = "a"

  print "@@nagios_service { \"check_vhost_"+DESC+'_${hostname}":'
  print "        use => \"generic-service\","
  print '        host_name => "$fqdn",'
  print "        service_description => \"VHOST "+URL+"\","
  print "        check_command => \"check_vhost!"+SERVERNAME+"!"+URI+"!80!1!2!5!"+CONTENTMATCH
  print "}"
  print 
  URI = None
#print A
#@@nagios_service { "check_vhost_rd1.ig.com.br_${hostname}":
#         use => "generic-service",
#         host_name => "$fqdn",
#         service_description => "VHOST rd1.ig.com.br",
#         check_command => "check_vhost!rd1.ig.com.br!/!80!1!2!5!a",
#   }
